import './App.css';
import { Switch, Route, BrowserRouter as Router } from 'react-router-dom';
import Conversations from './pages/Conversations';
import Landing from './pages/Landing';
import Messages from './pages/Messages';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={Landing} />
          <Route path="/conversations" exact component={Conversations} />
          <Route path="/messages/:conversationId" exact component={Messages} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
