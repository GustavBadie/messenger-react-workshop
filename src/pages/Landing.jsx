import { List, ListItem, TextField, Typography } from "@material-ui/core";

export default function Landing() {
    return (
        <main className="container" id="main">
            <Typography variant="h1">Welcome :D</Typography>
            <p>Thinghs we will do:</p>
            <List>
                <ListItem>Create components</ListItem>
                <ListItem>Get familliar with react famous libraries</ListItem>
                <ListItem>Connect our app to an api</ListItem>
            </List>
            <TextField label="Text" variant="outlined" type="text" />
        </main>
    )
}