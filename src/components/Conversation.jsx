import { Link } from "react-router-dom";
import PropTypes from 'prop-types';
export default function Conversation({ contact, lastMessage, date, conversationId }) {
    const styles = {
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        divider: {
            color: 'rgb(0, 0, 0)',
            backgroundColor: 'rgb(0, 0, 0)',
            height: '1px'
        },
        container: {
            marginTop: '1.22rem',
            marginBottom: '1.22rem'
        }
    }

    return (
        <Link to={`/messages/${conversationId}`}>
            <div style={styles.container}>
                <div style={styles.root}>
                    <div>
                        <h4>{contact}</h4>
                        <h5>{lastMessage}</h5>
                    </div>
                    <div>{date.toString()}</div>
                </div>
                <hr style={styles.divider} />
            </div>
        </Link>
    )
}

Conversation.propTypes = {
    contact: PropTypes.string,
    lastMessage: PropTypes.string,
    date: PropTypes.any,
    conversationId: PropTypes.number,
}

Conversation.defaultProps = {
    contact: '',
    lastMessage: '',
    date: new Date(),
    conversationId: 0,
}