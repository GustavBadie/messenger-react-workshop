import PropTypes from 'prop-types';

export default function Message({ status, from, text, date }) {

    const styles = {
        root: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        divider: {
            color: 'rgb(0, 0, 0)',
            backgroundColor: 'rgb(0, 0, 0)',
            height: '1px'
        },
        container: {
            marginTop: '1.22rem',
            marginBottom: '1.22rem'
        }
    }

    return (
        <div style={styles.container}>
            <div style={styles.root}>
                <div>
                    <h4>{from}</h4>
                    <h5>{text}</h5>
                    <small>{status}</small>
                </div>
                <div>{date.toString()}</div>
            </div>
            <hr style={styles.divider} />
        </div>
    )
}

Message.propTypes = {
    status: PropTypes.string,
    text: PropTypes.string,
    date: PropTypes.any,
    from: PropTypes.string,
}

Message.defaultProps = {
    status: '',
    text: '',
    date: new Date(),
    from: '',
}