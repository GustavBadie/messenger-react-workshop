const conversations = [
    {
        contact: 'Mamad Arabi',
        lastMessage: 'A',
        date: new Date()
    },
    {
        contact: 'Mamad Farsi',
        lastMessage: 'B',
        date: new Date()
    },
    {
        contact: 'Mamad French',
        lastMessage: 'C',
        date: new Date()
    },
    {
        contact: 'Mamad Australian',
        lastMessage: 'D',
        date: new Date()
    }
]

export default conversations;